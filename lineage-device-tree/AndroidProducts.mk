#
# Copyright (C) 2024 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_TECNO-LG6n.mk

COMMON_LUNCH_CHOICES := \
    lineage_TECNO-LG6n-user \
    lineage_TECNO-LG6n-userdebug \
    lineage_TECNO-LG6n-eng
