#
# Copyright (C) 2024 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from TECNO-LG6n device
$(call inherit-product, device/tecno/TECNO-LG6n/device.mk)

PRODUCT_DEVICE := TECNO-LG6n
PRODUCT_NAME := lineage_TECNO-LG6n
PRODUCT_BRAND := TECNO
PRODUCT_MODEL := TECNO LG6n
PRODUCT_MANUFACTURER := tecno

PRODUCT_GMS_CLIENTID_BASE := android-transsion

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="sys_tssi_64_tecno-user 12 SP1A.210812.016 67497 release-keys"

BUILD_FINGERPRINT := TECNO/LG6n-OP/TECNO-LG6n:12/SP1A.210812.016/220824V481:user/release-keys
